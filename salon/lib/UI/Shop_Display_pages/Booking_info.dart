import 'package:flutter/material.dart';

import 'booking_done_page.dart';
class bookingInfo extends StatefulWidget {
  const bookingInfo({Key? key}) : super(key: key);

  @override
  State<bookingInfo> createState() => _bookingInfoState();
}

class _bookingInfoState extends State<bookingInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Preview booking'),
      ),
      body:
        Column(
          children: [
            Container(
              height:40,
              decoration: BoxDecoration(
                  border: Border.all()
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text('Price',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black,)),
                  const Divider(
                    thickness: 2,
                  ),
                  Text('Time',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black,)),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  ListView.builder(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 10,
                        itemBuilder: (context, index) {
                          return Card(
                            margin: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: ListTile(
                                leading: CircleAvatar(
                                  backgroundImage: AssetImage('assets/images/salon.jpg'),
                                ),
                                title: Text('Cutting'),
                                subtitle: Text('2000'),
                                trailing: Icon(Icons.minimize,color: Colors.lightBlue,)
                            ),
                          );
                        }),
                  Container(
                      width: double.infinity,
                      child: ElevatedButton(onPressed: (){

                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const BookingDone()),
                        );

                      }, child: Text('Book',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white,)))
                  )

                ],
              ),
            ),
          ],
        )
    );
  }
}
